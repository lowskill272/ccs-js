export default {
  addToCart: (state, item) => {
    item.quantity = 1
    state.goods.push(item)
  },

  setQuantity: (state, payload) => {
    state.goods[payload.index].quantity = payload.value
  },

  increaseQuantity: (state, index) => {
    state.goods[index].quantity++
  },

  reduceQuantity: (state, index) => {
    state.goods[index].quantity--
  },

  removeFromCart: (state, index) => {
    state.goods.splice(index, 1)
  },

  clearCart: (state) => {
    state.goods = []
  },
}
